import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import {BrowserRouter as Router,Switch,Route} from 'react-router-dom'
import Home from './Component/Home';
import Menu from './Component/Menu';
import Add from './Component/Add';
import View1 from './Component/View1';
import Update from './Component/Update';
// import Delete from './Component/Delete';

function App() {
  return (
    <div>
      <Router>
         <Menu/>
         <Switch>
            <Route path='/' exact component={Home}/>
            <Route path='/add'  component={Add}/>
            <Route path='/view1/:id' component={View1}/>
            <Route path='/update/:id' component={Update}/>
            {/* <Route path='/delete/:id' component={Delete}/> */}
         </Switch>
      </Router> 
    </div>
  );
}

export default App;
