import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import axios from "axios";

export default class Update extends Component {
  constructor(props) {
    super(props);
    this.state = {
      title: "",
      description: "",
      img : "",
      id: props.match.params.id,
    };
  }
  componentWillMount() {
    let id = this.state.id;
    axios
      .get("http://110.74.194.124:15011/v1/api/articles/" + id)
      .then((res) => {
        // console.log(res.data.DATA.TITLE);
        this.setState({
          title: res.data.DATA.TITLE,
          description: res.data.DATA.DESCRIPTION,
          img : res.data.DATA.IMAGE,
        });
      });
    // console.log("work");
  }
  handleChange = (event) =>
  this.setState({ [event.target.name]: event.target.value });

  handleSubmit = (e) => {
    e.preventDefault();
    // console.log("work");
    let id = this.state.id;
    let title = this.title.value;
    let description = this.description.value;
    let data = {
      TITLE: title,
      DESCRIPTION: description,
    };
    axios
      .put("http://110.74.194.124:15011/v1/api/articles/" + id, data)
      .then((res) => {
        alert(res.data.MESSAGE);
        this.props.history.push("/");
      });
  };

  render() {
    return (
      <div className="container mt-3">
        <form onSubmit={this.handleSubmit} ref={(form) => (this.form = form)}>
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center bg-dark text-white">
                <h1>UPDATE ARTICLE</h1>
              </div>
            </div>
            <div className="row">
              <div className="col-lg-8">
                <Form.Group controlId="">
                  <Form.Label>TITLE</Form.Label>{" "}
                  <Form.Control
                    type="text"
                    value={this.state.title}
                    name="title"
                    onChange={this.handleChange}
                    ref={(inputTitle) => (this.title = inputTitle)}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Label>DESCRIPTION</Form.Label>
                  <Form.Control
                    type="text"
                    name="description"
                    value={this.state.description}
                    onChange={this.handleChange}
                    ref={(inputDescription) =>
                      (this.description = inputDescription)
                    }
                  />
                </Form.Group>
              </div>
              <div className="col-md-4">
                <img
                  width={300}
                  height={200}
                  className="ml-3"
                  src={this.state.img}
                  alt="image"
                />
              </div>
            </div>
            <div className="row">
              <Button variant="dark" type="submit" value="Submit">
                Save
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
