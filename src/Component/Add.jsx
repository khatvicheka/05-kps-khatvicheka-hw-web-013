import React, { Component } from "react";
import { Form, Button } from "react-bootstrap";
import axios from "axios";

export default class Add extends Component {
  handleSubmit = (e) => {
    e.preventDefault();
    let title = this.title.value;
    let description = this.description.value;
    if (title === "" && description === "") {
      alert("* Both Field Cannot Be Blank");
    } else {
      let data = {
        TITLE: title,
        DESCRIPTION: description,
      };
      axios
        .post("http://110.74.194.124:15011/v1/api/articles", data)
        .then((res) => {
          alert(res.data.MESSAGE);
          this.props.history.push("/");
        });
    }
  };

  render() {
    return (
      <div className="container mt-3">
        <form onSubmit={this.handleSubmit} ref={(form) => (this.form = form)}>
          <div className="container">
            <div className="row">
              <div className="col-lg-12 text-center bg-dark text-white">
                <h1>ADD ARTICLE</h1>
              </div>
            </div>
            <div className="row mt-3">
              <div className="col-lg-8">
                <Form.Group controlId="">
                  <Form.Label>TITLE</Form.Label>{" "}
                  <Form.Control
                    type="text"
                    ref={(inputTitle) => (this.title = inputTitle)}
                  />
                </Form.Group>

                <Form.Group controlId="">
                  <Form.Label>DESCRIPTION</Form.Label>
                  <Form.Control
                    type="text"
                    ref={(inputDescription) =>
                      (this.description = inputDescription)
                    }
                  />
                </Form.Group>
              </div>
              <div className="col-md-4">
                <img
                  width={300}
                  height={200}
                  className="ml-3"
                  // src="https://www.logaster.com/blog/wp-content/uploads/2018/05/LogoMakr.png"
                  // alt="Generic placeholder"
                />
              </div>
            </div>
            <div className="row">
              <Button variant="dark" type="submit" value="Submit">
                Submit
              </Button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
