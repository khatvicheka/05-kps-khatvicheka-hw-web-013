import React, { Component } from "react";
import axios from "axios";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";

export default class Home extends Component {
  constructor(props) {
    super(props); //since we are extending class Table so we have to use super in order to override Component class constructor
    this.state = {
      //state is by default an object
      students: [],
    };
  }
  componentWillMount() {
    axios
      .get("http://110.74.194.124:15011/v1/api/articles?page=1&limit=15")
      .then((res) => {
        this.setState({
          students: res.data.DATA,
        });
        // console.log(this.state.students);
      });
  }
  handleDelete = (e) => {
    // console.log(e.target.value);
    axios
      .delete(`http://110.74.194.124:15011/v1/api/articles/${e.target.value}`)
      .then((res) => {
        alert(res.data.MESSAGE);
        this.componentWillMount();
      })
      .catch((err) => {
        console.log(err);
      });
  };
  convertDate = (dateStr) => {
    let ds = dateStr;
    let year = ds.substring(0, 4);
    let month = ds.substring(4, 6);
    let day = ds.substring(6, 8);
    let date = year + "-" + month + "-" + day;
    return date;
  };

  renderTableData() {
    return this.state.students.map((student, index) => {
      const { ID, TITLE, DESCRIPTION, CREATE_DATE, IMAGE } = student; //destructuring
      return (
        <tr key={ID}>
          <td>{ID}</td>
          <td>{TITLE}</td>
          <td>{DESCRIPTION}</td>
          <td>{this.convertDate(CREATE_DATE)}</td>
          <td>
            <img src={IMAGE} style={{ width: "50px" }} alt="Image" />
          </td>
          <td className="Action">
            <Link as={Link} to={`/view1/${ID}`}>
              <Button variant="warning">View</Button>
            </Link>
            &nbsp;
            {/* <Link as={Link} to={`/add/isUpdate=true&&id=${ID}`}>
              <Button variant="warning">Update</Button>
            </Link> */}
            <Link as={Link} to={`/update/${ID}`}>
              <Button variant="warning">Update</Button>
            </Link>
            &nbsp;
            {/* <Link as={Link} to={`/${ID}`}> */}
            <Button
              variant="danger"
              onClick={this.handleDelete.bind(this)}
              value={ID}
            >
              Delete
            </Button>
            {/* </Link> */}
          </td>
        </tr>
      );
    });
  }

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-lg-12 text-center bg-dark text-white">
            <h1>REACT DYNAMIC TABLE</h1>
          </div>
        </div>
        <div className="row mt-3">
          <div className="col-lg-12 text-center">
            <Link as={Link} to="/add">
              <Button variant="dark">Add New Article</Button>
            </Link>
          </div>
        </div>
        {/* <Link as={Link} to="/add">
          <Button variant="dark">Add New Article</Button>
        </Link> */}

        <table className="table table-bordered mt-3">
          <thead>
            <tr>
              <th>#</th>
              <th>TITLE</th>
              <th>DESCRIPTION</th>
              <th>CREATE DATE</th>
              <th>IMAGE</th>
              <th>ACTION</th>
            </tr>
          </thead>
          <tbody>{this.renderTableData()}</tbody>
        </table>
      </div>
    );
  }
}
