import React, { Component } from "react";
import axios from "axios";

export default class View1 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Article: {},
      id: props.match.params.id,
    };
    // let idt = props.match.params.id;
  }
  componentWillMount() {
    let id = this.state.id;
    axios
      .get("http://110.74.194.124:15011/v1/api/articles/" + id)
      .then((res) => {
        console.log(res.data.DATA);
        this.setState({
          Article: res.data.DATA,
        });
        // console.log(this.state.Article);
      });
    // console.log("work");
  }
  // componentWillMount(props){
  //     let idt = props.match.params.id;
  //     console.log(idt);

  // }
  render() {
    return (
      <div className="container">
        <div className="card mb-3">
          <div className="row no-gutters">
            <div className="col-md-4">
              <img src={this.state.Article.IMAGE} class="card-img" alt="..." />
            </div>
            <div className="col-md-8">
              <div className="card-body">
                <h5 className="card-title">{this.state.Article.TITLE}</h5>
                <p className="card-text">{this.state.Article.DESCRIPTION}</p>
                <p className="card-text">
                  <small className="text-muted">Last updated 3 mins ago</small>
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
